const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const pointSchema = new Schema({
    lat: {
        type: Number,
        required: true
    },
    lng: {
        type: Number,
        required: true
    },
    timestamp: {
        type: Number,
        required: true
    }
});

mongoose.model('point', pointSchema);