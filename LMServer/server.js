//===============
//Dependencies ==
//===============
const config = require('./config')
const express = require('express');
const mongoose = require('mongoose');
const Joi = require('joi');
const validation = require('./validation');
const fs = require('fs');
fs.readdirSync(__dirname + '\\models').forEach((filename) => {
    if(~filename.indexOf('.js')) require(__dirname + '\\models\\' + filename);
});

//=========================
//Database Configuration ==
//=========================
mongoose.connect(config.database, {useNewUrlParser: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error'));
db.once('open', (callback) => {
    console.log('Connection succeeded');
});

//============================
//Application Configuration ==
//============================
const app = express();
app.use(express.json());
const port = process.env.PORT || config.port;
const server = app.listen(port, () => {
    console.log(`Listening on port ${port}...`);
});

//============
//Endpoints ==
//============
app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.post('/position/', (req, res) => {
    const {error} = validatePoint(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const Point = mongoose.model('point');
    const newPoint = new Point();
    newPoint.lat = req.body.lat;
    newPoint.lng = req.body.lng;
    newPoint.timestamp = req.body.timestamp;
    newPoint.save((err) => {
        if(err) throw err;
        console.log(`Added new Point with lat: "${newPoint.lat}" lng: "${newPoint.lng}" timestamp: "${newPoint.timestamp}"`);
        res.send(newPoint);
    });
});

function validatePoint(point) {
    return Joi.validate(point, validation.point);
}