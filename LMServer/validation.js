const Joi = require('joi');

const pointSchema = {
    lat: Joi.number().required(),
    lng: Joi.number().required(),
    timestamp: Joi.number().required()
}

module.exports.point = pointSchema;