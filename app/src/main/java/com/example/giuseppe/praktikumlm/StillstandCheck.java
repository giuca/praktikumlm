package com.example.giuseppe.praktikumlm;

public class StillstandCheck {

    private double oldLat;
    private double oldLng;

    public StillstandCheck(double oldLat, double oldLng) {
        this.oldLat = oldLat;
        this.oldLng = oldLng;
    }

    public double getOldLat() {
        return oldLat;
    }

    public void setOldLat(double oldLat) {
        this.oldLat = Math.round(oldLat*10000.)/10000.;
    }

    public double getOldLng() {
        return oldLng;
    }

    public void setOldLng(double oldLng) {
        this.oldLng = Math.round(oldLng*10000.)/10000.;
    }

    protected boolean checkStillstand(double currentLat, double currentLng){

        //Koordinaten auf 4 Nachkommerstellen beschränken
        currentLat = Math.round(currentLat*10000.)/10000.;
        currentLng = Math.round(currentLng*10000.)/10000.;

        if (currentLat == oldLat && currentLng == oldLng){
            return true;
        }else {
            return false;
        }
    }
}
