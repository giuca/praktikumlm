package com.example.giuseppe.praktikumlm;
import android.app.Activity;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.LinkedList;
public class MySensor {

    private JSONArray gps_json;

  public void toJSON(LinkedList<JSONObject> sensor_list, Activity activity) {
    gps_json = new JSONArray();
      for (JSONObject obj: sensor_list){
          try {
              if (obj.getString("Sensor").compareTo("GPS") == 0){
                  gps_json.put(obj);
              }
          }catch (JSONException e){
              Log.e("MySensor ERROR","JSON failed");
          }
      }
      saveSensor(activity);
  }

  private void saveSensor(Activity activity){
      SaveJSON.saveToFile(gps_json.toString(),activity,"gps.json");
  }

}
