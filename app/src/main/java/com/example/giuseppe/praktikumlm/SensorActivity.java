package com.example.giuseppe.praktikumlm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;


public class SensorActivity extends AppCompatActivity {

    GUISensorComponents guiAccelerometerComponents;
    GUISensorComponents guiGyroscopeComponents;
    GUISensorComponents guiMagneticFieldComponents;
    GUISensorComponents guiProximityComponents;
    GUISensorComponents guiLightSensorComponents;

    boolean accelOn = false;
    boolean gyroOn = false;
    boolean lightOn = false;
    boolean magnetOn = false;
    boolean proximityOn = false;
    SensorManager sensorMan;

    /*MySensor saveMe = new MySensor();
    LinkedList<JSONObject> sensor_list = new LinkedList<>();
    */

    MySensorEventListener accelListener;
    MySensorEventListener gyroListener;
    MySensorEventListener lightListener;
    MySensorEventListener magnetListener;
    MySensorEventListener proximityListener;
    HashMap<Integer, MySensorEventListener> mySensorEventListeners;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

                for (int id : mySensorEventListeners.keySet()) {
                    updateGraphview(id, mySensorEventListeners.get(id));
                }

            handler.postDelayed(this, 1000);
        }
    };

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        sensorMan = (SensorManager) getSystemService(SENSOR_SERVICE);

        initializeComponents();
        String[] accelBez = {"x", "y", "z"};
        String[] gyroBez = {"x", "y", "z"};
        String[] lightBez = {"x"};
        String[] magnetBez = {"x", "y", "z"};
        String[] proximityBez = {"x"};
        accelListener = new MySensorEventListener("Accelerometer", accelBez, guiAccelerometerComponents);
        gyroListener = new MySensorEventListener("Gyroscope", gyroBez, guiGyroscopeComponents);
        lightListener = new MySensorEventListener("Light", lightBez, guiLightSensorComponents);
        magnetListener = new MySensorEventListener("MagnetField", magnetBez, guiMagneticFieldComponents);
        proximityListener = new MySensorEventListener("Proximity", proximityBez, guiProximityComponents);

        mySensorEventListeners = new HashMap<>();
        mySensorEventListeners.put(R.id.accelGraphView, accelListener);
        mySensorEventListeners.put(R.id.gyroGraphView, gyroListener);
        mySensorEventListeners.put(R.id.lightGraphView, lightListener);
        mySensorEventListeners.put(R.id.magGraphView, magnetListener);
        mySensorEventListeners.put(R.id.proxGraphView, proximityListener);

        startApplication();
        handler.post(runnable);

    }

    /*
     * Initialize needed GUI Components
     */
    public void initializeComponents() {

        guiAccelerometerComponents = new GUISensorComponents(this, R.id.acceloremeterTV,
                R.id.accelerometerSwitch,
                R.id.accelerometerResultTV);

        guiGyroscopeComponents = new GUISensorComponents(this, R.id.gyroscopeTV,
                R.id.gyroscopeSwitch,
                R.id.gyroscopeResultTV);

        guiMagneticFieldComponents = new GUISensorComponents(this, R.id.magneticField,
                R.id.magneticFieldSwitch,
                R.id.magneticFieldResultTV);

        guiProximityComponents = new GUISensorComponents(this, R.id.proximityTV,
                R.id.proximitySwitch,
                R.id.proximityResultTV);

        guiLightSensorComponents = new GUISensorComponents(this, R.id.lightSensorTV,
                R.id.lightSensorSwitch,
                R.id.lightSensorResultTV);
    }

    public void startApplication(){
        // Switch Listener
        CompoundButton.OnCheckedChangeListener multiListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                switch (v.getId()) {
                    case R.id.accelerometerSwitch:
                        if (!accelOn) {
                            sensorMan.registerListener(accelListener, sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                        } else {
                            sensorMan.unregisterListener(accelListener);
                            accelListener.saveToFile();
                        }
                        accelOn = !accelOn;
                        break;
                    case R.id.gyroscopeSwitch:
                        if (!gyroOn) {
                            sensorMan.registerListener(gyroListener, sensorMan.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
                        } else {
                            sensorMan.unregisterListener(gyroListener);
                            gyroListener.saveToFile();
                        }
                        gyroOn = !gyroOn;
                        break;
                    case R.id.lightSensorSwitch:
                        if (!lightOn) {
                            sensorMan.registerListener(lightListener, sensorMan.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL);
                        } else {
                            sensorMan.unregisterListener(lightListener);
                            lightListener.saveToFile();
                        }
                        lightOn = !lightOn;
                        break;
                    case R.id.magneticFieldSwitch:
                        if (!magnetOn){
                            sensorMan.registerListener(magnetListener, sensorMan.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);
                        }
                        else {
                            sensorMan.unregisterListener(magnetListener);
                            magnetListener.saveToFile();
                        }
                        magnetOn = !magnetOn;
                        break;
                    case R.id.proximitySwitch:
                        if (!proximityOn) {
                            sensorMan.registerListener(proximityListener, sensorMan.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                        }
                        else {
                            sensorMan.unregisterListener(proximityListener);
                            proximityListener.saveToFile();
                        }
                        proximityOn = !proximityOn;
                        break;
                }
            }
        };
        ((Switch) findViewById(R.id.gyroscopeSwitch)).setOnCheckedChangeListener(multiListener);
        ((Switch) findViewById(R.id.accelerometerSwitch)).setOnCheckedChangeListener(multiListener);
        ((Switch) findViewById(R.id.lightSensorSwitch)).setOnCheckedChangeListener(multiListener);
        ((Switch) findViewById(R.id.magneticFieldSwitch)).setOnCheckedChangeListener(multiListener);
        ((Switch) findViewById(R.id.proximitySwitch)).setOnCheckedChangeListener(multiListener);
    }

    /**
     * aktualisiert den Graphen des Sensors
     * feuert jedes Mal bei OnSensorChanged
     * @param graphID ID des GraphViews
     */
    public void updateGraphview(final int graphID, final MySensorEventListener mySensorEventListener) {
        try {
            DataPoint[] dataPoints;
            dataPoints = convertFloatArrToDPArr(mySensorEventListener.getSensorValues());
            GraphView graphView = findViewById(graphID);
            graphView.getViewport().setXAxisBoundsManual(true);
            graphView.getViewport().setMinX(0);
            graphView.getViewport().setMaxX(3);

            graphView.getViewport().setYAxisBoundsManual(true);
            graphView.getViewport().setMaxY(30);
            graphView.getViewport().setMinY(-30);

            graphView.removeAllSeries();
            BarGraphSeries<DataPoint> series;
            series = new BarGraphSeries<>(dataPoints);

            series.setSpacing(10);
            graphView.addSeries(series);
        }catch (NoSuchElementException e){

        }


    }

    /**
     * Wandelt einen float[] in Datapoint[] um
     * @param floats float[]
     * @return einen DataPoint[]
     */
    public DataPoint[] convertFloatArrToDPArr(float[] floats){
        DataPoint[] dataPoints = new DataPoint[floats.length];
        for(int i = 0; i < floats.length; i++){
            dataPoints[i] = new DataPoint(i, floats[i]);
        }
        return dataPoints;
    }


}
