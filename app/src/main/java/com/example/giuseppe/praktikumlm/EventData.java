package com.example.giuseppe.praktikumlm;

public class EventData {
    public final long timestamp;
    public final float[] values;

    public EventData(long timestamp, float[] values) {
        this.timestamp = timestamp;
        this.values = values.clone();
    }
}
