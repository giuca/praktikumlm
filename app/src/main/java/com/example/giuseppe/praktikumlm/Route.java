package com.example.giuseppe.praktikumlm;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

class Route {

    private ArrayList<LatLng> milestoneArray = new ArrayList<>();

    /*Route(){
        milestoneArray.add(new LatLng( 51.532574, 7.659194 ));
        milestoneArray.add(new LatLng( 51.532649, 7.659850 ));
        milestoneArray.add(new LatLng( 51.534350, 7.660358 ));
        milestoneArray.add(new LatLng( 51.535245, 7.660182 ));
        milestoneArray.add(new LatLng( 51.535300, 7.659313 ));
        milestoneArray.add(new LatLng( 51.535184, 7.659270 ));
        milestoneArray.add(new LatLng( 51.535137, 7.659128 ));
        milestoneArray.add(new LatLng( 51.535289, 7.657808 ));
        milestoneArray.add(new LatLng( 51.535188, 7.656177 ));
        milestoneArray.add(new LatLng( 51.535215, 7.655155 ));
        milestoneArray.add(new LatLng( 51.531948, 7.656185 ));
        milestoneArray.add(new LatLng( 51.532045, 7.657518 ));
        milestoneArray.add(new LatLng( 51.532554, 7.659069 ));
    }*/

    Route(){
        milestoneArray.add(new LatLng( 51.52574, 7.14356 ));    // A
        milestoneArray.add(new LatLng( 51.52538, 7.14261 ));    // B
        milestoneArray.add(new LatLng( 51.52624, 7.14174 ));    // C
        milestoneArray.add(new LatLng( 51.52713, 7.14109 ));    // D
        milestoneArray.add(new LatLng( 51.52710, 7.14040 ));    // E
        milestoneArray.add(new LatLng( 51.52771, 7.14024 ));    // F
        milestoneArray.add(new LatLng( 51.52824, 7.14026 ));    // G
        milestoneArray.add(new LatLng( 51.52781, 7.14170 ));    // H
        milestoneArray.add(new LatLng( 51.52739, 7.14225 ));    // I
        milestoneArray.add(new LatLng( 51.52690, 7.14318 ));    // J
        milestoneArray.add(new LatLng( 51.52626, 7.14445 ));    // K
    }


    /**
     * Initialisiert eine Route in eine ArrayList aus LatLng
     * @return eine ArrayList mit Etappen
     */
    ArrayList<LatLng> initializeRoute(){
        return new ArrayList<>(milestoneArray);
    }
}
