package com.example.giuseppe.praktikumlm;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This class is used to create a new GUI Component for a Sensor
 *
 */
public class GUISensorComponents {

    private Activity activity;
    private TextView nameTV;
    private Switch switchComp;
    private TextView resultTV;
    private Button button;
    private Button startRouteButton;
    private Button stopRouteButton;
    private RadioGroup radioGroup;
    private int seekBarValue;
    private int seekBar2Value;

    /**
     *
     * @param activity The main activity
     * @param sensorNameID ID of Name TextView
     * @param switchID ID of Switch
     * @param resultID ID of Result TextView
     */
    GUISensorComponents(@NonNull Activity activity, int sensorNameID, int switchID, int resultID){
        this.activity = activity;
        this.nameTV = activity.findViewById(sensorNameID);
        this.switchComp = activity.findViewById(switchID);
        this.resultTV = activity.findViewById(resultID);
    }

    /**
     *
     * @return name TextView of Sensor
     */
    public TextView getNameTV(){

        return nameTV;
    }

    /**
     *
     * @return switch of Sensor
     */
    public Switch getSwitchComp(){
        return switchComp;
    }

    /**
     *
     * @return result TextView of Sensor
     */
    public TextView getResultTV(){

        return resultTV;
    }

    /**
     * Sets name of sensor
     * @param name name of Sensor
     */
    void setNameTV(String name){
        nameTV.setText(name);
    }

    /**
     * sets result of sensor
     * @param result result as string for TextView
     */
    void setResultTV(String result){

        resultTV.setText(result);
    }

    /**
     * initialize ShowMapButton
     * @param buttonId ID of ShowMapButton to be used
     */
    void setShowMapButton(int buttonId){
        this.button = activity.findViewById(buttonId);
    }

    /**
     *
     * @return ShowMapButton of GuiComponent
     */
    Button getShowMapButton(){
        if(button != null){
            return button;
        }
        else{
            Toast toast = Toast.makeText(activity, "No Button initialized", Toast.LENGTH_LONG);
            toast.show();
            return null;
        }
    }

    /**
     *
     * @return startRouteButton
     */
    public Button getStartRouteButton() {
        return startRouteButton;
    }

    /**
     * initializes startRouteButton
     * @param buttonId ID of StartRouteButton
     */
    public void setStartRouteButton(int buttonId) {
        this.startRouteButton = activity.findViewById(buttonId);
    }

    /**
     * sets the Text of the Start Button
     * @param text text to show
     */
    void setStartRouteButtonText(String text){
        startRouteButton.setText(text);
    }

    /**
     *
     * @return stopRouteButton
     */
    public Button getStopRouteButton() {
        return stopRouteButton;
    }

    /**
     * initializes stopRouteButton
     * @param buttonId ID of StopRouteButton
     */
    public void setStopRouteButton(int buttonId) {
        this.stopRouteButton = activity.findViewById(buttonId);
    }

    public void setRadioGroup(int radioGroupID){
        this.radioGroup = activity.findViewById(radioGroupID);
    }

    public RadioGroup getRadioGroup(){ return radioGroup; }

    /**
     * Erstellt Seekbar mit vorgegebenen Werten. Wert der Seekbar kann mit
     * getSeekBarValue() abgefragt werden.
     * @param initialValue Startwert der Seekbar
     * @param maxValue Maximaler Wert der Seekbar in Sekunden
     * @param stepFactor Schrittweite der Seekbar
     * @param valueUnit Einheit die in der Textview angehängt werden soll
     */
    void createSeekBar(int initialValue, int maxValue, final int stepFactor, final String valueUnit){
        SeekBar seekBar = activity.findViewById(R.id.seekBar);
        seekBar.setProgress(initialValue);
        seekBar.setMax(maxValue/stepFactor);
        final TextView tvSeekBarValue = activity.findViewById(R.id.textViewSeekBar);
        String tmp = String.valueOf(0) + valueUnit;
        tvSeekBarValue.setText(tmp);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarValue = progress * stepFactor;
                String tmp = String.valueOf(seekBarValue) + valueUnit;
                tvSeekBarValue.setText(tmp);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * Gibt Wert der Seekbar zurück
     * @return Wert der Seekbar
     */
    int getSeekBarValue() {
        return seekBarValue;
    }

        // Zweite Seekbar für Distance Based m/s
    void createSeekBar2(int initialValue, int maxValue, final int stepFactor, final String valueUnit){
        SeekBar seekBar2 = activity.findViewById(R.id.seekBar2);
        seekBar2.setProgress(0);
        seekBar2.setMax(maxValue/stepFactor);
        final TextView tvSeekBar2Value = activity.findViewById(R.id.textViewSeekBar2);
        String tmp = String.valueOf(0) + valueUnit;
        tvSeekBar2Value.setText(tmp);

        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBar2Value = progress * stepFactor;
                String tmp = String.valueOf(seekBar2Value) + valueUnit;
                tvSeekBar2Value.setText(tmp);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar2) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar2) {

            }
        });
    }

    int getSeekBar2Value() {
        return seekBar2Value;
    }



}
