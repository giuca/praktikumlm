package com.example.giuseppe.praktikumlm;

import android.app.Activity;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SaveJSON {

    public static boolean saveToFile(String data, Activity activity, String fileName){

        try{
            Context context;
            context = activity.getApplicationContext();

            File file = new File(context.getExternalFilesDir(null),fileName);
            FileOutputStream fos = new FileOutputStream(file,true);
            fos.write((data).getBytes());
            fos.close();

            MediaScannerConnection.scanFile(context, new String[] {file.toString()}, null, null);

            return true;

        }catch (FileNotFoundException e){
            Log.e("SaveJSON ERROR","Datei nicht gefunden");
        }catch (IOException e){
            Log.e("SaveJSON ERROR","FOS error");
        }
        return false;
    }

}

