package com.example.giuseppe.praktikumlm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button buttonTimeBasedAct;
    Button buttonDistanceBasedAct;
    Button buttonSensorAct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonTimeBasedAct = findViewById(R.id.button_timeBased);
        buttonDistanceBasedAct = findViewById(R.id.button_distanceBased);
        buttonSensorAct = findViewById(R.id.button_sensorAct);

        buttonTimeBasedAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyActivity(TimeBasedActivity.class);
            }
        });

        buttonDistanceBasedAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyActivity(DistanceBasedActivity.class);
            }
        });

        buttonSensorAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyActivity(SensorActivity.class);
            }
        });
    }

    public void startMyActivity(Class activity){
        Intent intent = new Intent(MainActivity.this, activity);
        startActivity(intent);
    }
}
