package com.example.giuseppe.praktikumlm;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    LatLng src;
    LatLng dst;
    ArrayList<LatLng> mapPositionList = new ArrayList<>();
    ArrayList<LatLng> interPositionList = new ArrayList<>();
    ArrayList<LatLng> gpsPositionsList = new ArrayList<>();

    int mapCoordinateSize;
    int interpolatedCoordinates;
    int gpsCoordinateSize;


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync( this);

        Intent intent = this.getIntent();


        //double[] srcArray = intent.getDoubleArrayExtra("src");
        //double[] dstArray = intent.getDoubleArrayExtra("dst");
        //src = new LatLng(srcArray[0], srcArray[1]);
        //dst = new LatLng(dstArray[0], dstArray[1]);

        mapCoordinateSize = intent.getIntExtra("mapCoordinatesSize", 0);

        for(int i = 0; i < mapCoordinateSize; i++){
            double[] positionArray = intent.getDoubleArrayExtra("mapValue"+i);
            mapPositionList.add(new LatLng(positionArray[0], positionArray[1]));
        }


        gpsCoordinateSize = intent.getIntExtra("gpsCoordinatesSize", 0);

        for(int i = 0; i < gpsCoordinateSize; i++){
            double[] positionArray = intent.getDoubleArrayExtra("gpsValue"+i);
            gpsPositionsList.add(new LatLng(positionArray[0], positionArray[1]));
        }

        interpolatedCoordinates = intent.getIntExtra("interpolatedCoordinatesSize", 0);

        for(int i = 0; i < interpolatedCoordinates; i++){
            double[] positionArray = intent.getDoubleArrayExtra("interValue"+i);
            interPositionList.add(new LatLng(positionArray[0], positionArray[1]));
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //mMap.addMarker(new MarkerOptions().position(src).title("Start"));
        for(int i = 0; i < mapPositionList.size(); i++){
            Log.d("mappositions", "onMapReady: " + mapPositionList.get(i));
            mMap.addMarker(new MarkerOptions().position(mapPositionList.get(i)).title("milestone" + i));
        }
        //mMap.addMarker(new MarkerOptions().position(dst).title("End"));

        mMap.addPolyline(new PolylineOptions().addAll(mapPositionList).color(Color.BLUE));

        for(int i = 0; i < interPositionList.size(); i++){
            mMap.addCircle(new CircleOptions().center(interPositionList.get(i)).radius(2).fillColor(Color.GREEN));
        }

        for(int i = 0; i < gpsPositionsList.size(); i++){
            mMap.addCircle(new CircleOptions().center(gpsPositionsList.get(i)).radius(2).fillColor(Color.BLUE));
        }

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(gpsPositionsList.get(0), 17);
        mMap.animateCamera(cameraUpdate);

    }


}
