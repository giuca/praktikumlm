package com.example.giuseppe.praktikumlm;

import android.location.Location;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.Collections;

class CalculationClass {

    private ArrayList<DataPoint> dataPointArrayList = new ArrayList<>();

    /**
     * interpolates between a LatLng source and destination
     * @param src source location
     * @param dst destination location
     * @param timeStart starting time
     * @param timeEnd end time
     */
    void linearCoordinatesInterpolation(LatLng src, LatLng dst, long timeStart, long timeEnd, double gpsTimerDelayMillis, ArrayList<LatLng> interpolatedCoordinates){
        double deltaLat = dst.latitude - src.latitude;
        double deltaLong = dst.longitude - src.longitude;
        double routeTime = timeEnd - timeStart;
        double currentTime = timeStart + gpsTimerDelayMillis;

        while(currentTime < timeEnd){
            double deltaTime = (currentTime - timeStart) / routeTime;
            double newLat = src.latitude + deltaLat * deltaTime;
            double newLng = src.longitude + deltaLong * deltaTime;
            LatLng newCoordinate = new LatLng(newLat, newLng);
            interpolatedCoordinates.add(newCoordinate);
            Log.d("new Coordinate", "linearCoordinatesInterpolation: " + newCoordinate);

            currentTime = currentTime + gpsTimerDelayMillis;
        }
    }

    /**
     * Computes location error between two ArrayLists
     * @param interpolatedList first ArrayList<LatLng>
     * @param gpsCoordinates second ArrayList<LatLng>
     */
    void computeError(ArrayList<LatLng> interpolatedList, ArrayList<LatLng> gpsCoordinates){
        ArrayList<Integer> distanceArray = new ArrayList<>();
        Log.d("BLA", "computeError: " + interpolatedList.size() + " / " + gpsCoordinates.size());
        for(int i = 0; i < interpolatedList.size(); i++){
            Location interpolatedLocation = new Location("interpolated");

            try {
                interpolatedLocation.setLatitude(interpolatedList.get(i).latitude);
                interpolatedLocation.setLongitude(interpolatedList.get(i).longitude);
            }catch (IndexOutOfBoundsException e){
                break;
            }
            Location gpsLocation;
            try {
                gpsLocation = new Location("gps");
                gpsLocation.setLatitude(gpsCoordinates.get(i).latitude);
                gpsLocation.setLongitude(gpsCoordinates.get(i).longitude);
            }catch(IndexOutOfBoundsException e){
                break;
            }
            int distance = (int)interpolatedLocation.distanceTo(gpsLocation);
            distanceArray.add(distance);

            Log.d("distance", "computeError: " + distance);
        }

        Collections.sort(distanceArray);

        for(int i = 0; i < distanceArray.size(); i++) {
            dataPointArrayList.add(new DataPoint(distanceArray.get(i), getRelativeAmountOfError(distanceArray, distanceArray.get(i))));
        }
        //createCDFGraph(dataPointArrayList);
    }

    ArrayList<DataPoint> getDataPointArrayList(){
        return dataPointArrayList;
    }

    /**
     * Computes relative error amount
     * @param intArray array to be checked
     * @param number number to be checked
     * @return relative amount of number in array
     */
    private float getRelativeAmountOfError(ArrayList<Integer> intArray, int number){
        int count = 0;
        for (int i = 0; i < intArray.size(); i++) {
            if(number >= intArray.get(i)){
                count++;
            }
        }
        return ((float)count / (float)intArray.size()) * 100.0f;
    }
}
