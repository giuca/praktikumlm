package com.example.giuseppe.praktikumlm;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class DistanceBasedActivity extends AppCompatActivity {

    GUISensorComponents guiGPSComponents;
    CalculationClass errorCalc = new CalculationClass();

    boolean gpsCheck = false;
    boolean started = false;


    SensorManager sensorMan;
    LocationManager locMan;

    RadioGroup guiRadioGroup;
    RadioButton radioButton;
    int gpsMode;
    public LocationRequest locationRequest;
    // GeoCoder geocoder;

    MySensor saveMe = new MySensor();
    LinkedList<JSONObject> sensor_list = new LinkedList<>();

    JSONObject gps_json;
    StillstandCheck stillstandCheck = new StillstandCheck(0,0);

    ArrayList<LatLng> interpolatedCoordinates = new ArrayList<>();
    ArrayList<LatLng> mapCoordinates = new ArrayList<>();

    Route route = new Route();

    long timeStart;
    long timeEnd;
    ArrayList<LatLng> gpsCoordinates = new ArrayList<>();

    Location location;
    LocationListener locListener;
    Location oldLocation = new Location("oldLocation");
    Location milestoneBufferA = new Location ("milestoneBufferA");
    Location milestoneBufferB = new Location ("milestoneBufferB");

    int gpsTimerDelayMillis = 1000;
    double gpsTimerDelayMeterSek = 1000;
    boolean okToAdd = true;

    Handler gpsTimerHandler = new Handler();
    Runnable gpsTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if(okToAdd) {
                gpsCoordinates.add(new LatLng(location.getLatitude(), location.getLongitude()));
                Toast toast = Toast.makeText(DistanceBasedActivity.this, "Neuer fix!", Toast.LENGTH_SHORT);
                toast.show();
                okToAdd = false;
            }
            gpsTimerDelayMeterSek = ((double)guiGPSComponents.getSeekBarValue()/guiGPSComponents.getSeekBar2Value())*1000;
            gpsTimerHandler.postDelayed(this, (int)gpsTimerDelayMeterSek);
          //  Toast toast2 = Toast.makeText(DistanceBasedActivity.this, "Warte für soviel ms: " + (int) gpsTimerDelayMeterSek , Toast.LENGTH_SHORT);
          //  toast2.show();

        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_based);

        locMan = (LocationManager) getSystemService(LOCATION_SERVICE);
        sensorMan = (SensorManager) getSystemService(SENSOR_SERVICE);

        mapCoordinates = route.initializeRoute();

        gpsMode = 105;
        if(!interpolatedCoordinates.isEmpty()){
            interpolatedCoordinates.clear();
        }

        initializeComponents();

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        } else {
            startApplication();
        }
    }

    /*
     * Initialize needed GUI Components
     */
    public void initializeComponents() {
        guiGPSComponents = new GUISensorComponents(this, R.id.gpsTV,
                R.id.gpsSwitch,
                R.id.gpsResultTV);

        //guiGPSComponents.setRadioGroup(R.id.radioGroup);
        guiRadioGroup = findViewById(R.id.radioGroup);

        guiGPSComponents.setShowMapButton(R.id.startMapButton);
        addShowMapButtonListener(guiGPSComponents.getShowMapButton());
        guiGPSComponents.setStartRouteButton(R.id.startRouteButton);
        addStartRouteButtonListener(guiGPSComponents.getStartRouteButton());
        guiGPSComponents.setStopRouteButton(R.id.stopRouteButton);
        addStopRouteButtonListener(guiGPSComponents.getStopRouteButton());
        guiGPSComponents.createSeekBar(0,20,5 ,"m");
        guiGPSComponents.createSeekBar2(0,20,1 ,"m/s");

    }

    /**
     * startet die MapActivity und übergibt die Koordinaten, die in interpolatedCoordinates gespeichert sind
     *
     * @param interpolatedCoordinates eine ArrayList<Lat, Long>
     */
    public void startMapActivity(ArrayList<LatLng> interpolatedCoordinates, ArrayList<LatLng> gpsCoordinates) {
        Intent intent = new Intent(DistanceBasedActivity.this, MapsActivity.class);

        convertArrayListForIntent(intent, "mapCoordinates", "mapValue", mapCoordinates);
        convertArrayListForIntent(intent, "interpolatedCoordinates", "interValue", interpolatedCoordinates);
        convertArrayListForIntent(intent, "gpsCoordinates", "gpsValue", gpsCoordinates);

        startActivity(intent);
        interpolatedCoordinates.clear();
    }

    public void startApplication() {

        // Location Listener
        locListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {          // Wait(guiGPSComponents.getSeekBar2Value) ?
                gps_json = new JSONObject();

                if (gpsMode==0 && gpsCheck) {
                    guiGPSComponents.setResultTV("Positionsdaten"
                            + "\nLatitude: " + location.getLatitude()
                            + "\nLongitude: " + location.getLongitude()
                            + "\nHöhe: " + location.getAltitude()
                            + "\nGeschwindigkeit: " + location.getSpeed()
                            + "\nGenauigkeit: " + location.getAccuracy()
                    );
                    DistanceBasedActivity.this.location = location;
                    if(!stillstandCheck.checkStillstand(location.getLatitude(),location.getLongitude())){   // && checkStillstand.distanceTo(location) >= guiGPSComponents.getSeekBarValue()
                        stillstandCheck.setOldLat(location.getLatitude());
                        stillstandCheck.setOldLng(location.getLongitude());
                        if(location.distanceTo(oldLocation)>= guiGPSComponents.getSeekBarValue()) {
                            try {
                                gps_json.put("Sensor", "GPS");
                                gps_json.put("lat", location.getLatitude());
                                gps_json.put("long", location.getLongitude());
                                gps_json.put("alt", location.getAltitude());
                                gps_json.put("Datum", createDate(System.currentTimeMillis()));
                                gps_json.put("Timestamp", createTimestamp(System.currentTimeMillis()));
                                sensor_list.add(gps_json);
                                JSONObject postthis = new JSONObject();
                                postthis.put("lat", location.getLatitude());
                                postthis.put("lng", location.getLongitude());
                                postthis.put("timestamp", location.getTime());
                                PostTask p = new PostTask();
                                p.execute(postthis.toString());
                            } catch (JSONException e) {
                                Log.e("Error", "error");
                            }
                            saveMe.toJSON(sensor_list, DistanceBasedActivity.this);
                            okToAdd = true;
                            oldLocation.setLatitude(location.getLatitude());
                            oldLocation.setLongitude(location.getLongitude());
                        } else {
                        //    Toast toast = Toast.makeText(DistanceBasedActivity.this, "Distanz zum letzten Punkt weniger als " + guiGPSComponents.getSeekBarValue() + "m.", Toast.LENGTH_SHORT);
                         //   toast.show();
                        }

                    }

                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        // fuseBasedLocation
        locationRequest = new LocationRequest();
        //    locationRequest.setPriority(gpsMode);
        locationRequest.setInterval(2000);
        locationRequest.setFastestInterval(1000);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }

        getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if(gpsMode!=0 && gpsCheck) {
                    Toast.makeText(DistanceBasedActivity.this, "fuseBased", Toast.LENGTH_SHORT).show();
                    fusedOnLocationChanged(locationResult.getLastLocation());
                }
            }
        }, Looper.myLooper());


        // Switch Listener
        CompoundButton.OnCheckedChangeListener multiListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                switch (v.getId()) {
                    case R.id.gpsSwitch:
                        checkSensors("gps");
                        guiGPSComponents.setNameTV("GPS mode:" + gpsMode + "\n" +gpsCheck);
                        break;
                }
            }
        };

        ((Switch) findViewById(R.id.gpsSwitch)).setOnCheckedChangeListener(multiListener);
    }

    private void fusedOnLocationChanged(Location location) {
        //     List<Address> adressdaten = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        //     Address address = adressdaten.get(0);
        //     String adresszeile = address.getAddressLine(0);
        guiGPSComponents.setResultTV("Latitude: " + location.getLatitude() +
                "\nLongitude: " + location.getLongitude() +
                "\nAltitude: " + location.getAltitude() +
                "\nSpeed: " + location.getSpeed() );
    }

    public void checkSensors(final String sensorName) {
        switch(sensorName) {
            case "gps":
                if (!gpsCheck) {
                    gpsTimerDelayMillis = guiGPSComponents.getSeekBarValue() * 1000;
                    if (ActivityCompat.checkSelfPermission(DistanceBasedActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DistanceBasedActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, gpsTimerDelayMillis, 0, locListener);
                    locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, gpsTimerDelayMillis, 0, locListener);
                    guiGPSComponents.setResultTV("searching...");
                    gpsCheck = true;
                } else {
                    locMan.removeUpdates(locListener);
                    guiGPSComponents.setResultTV("OFF");
                    gpsCheck = false;
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(allPermissionsGranted(grantResults)) {
            startApplication();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    public boolean allPermissionsGranted(int[] grantedPermissions){
        for(int grantedPermission : grantedPermissions){
            if(grantedPermission == PackageManager.PERMISSION_DENIED){
                return false;
            }
        }
        return true;
    }

    /**
     * Erstellt CDFGraphen mit einer ArrayList<DataPoint> im UI-Thread
     * @param dataPointArrayList Die zu übergebende ArrayList<DataPoint>
     */
    public void createCDFGraph(final ArrayList<DataPoint> dataPointArrayList){
        final Activity activity = DistanceBasedActivity.this;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                GraphView graph = findViewById(R.id.graph);
                graph.getViewport().setXAxisBoundsManual(true);
                graph.getViewport().setYAxisBoundsManual(true);
                graph.getViewport().setMinX(0);
                graph.getViewport().setMinY(0);
                graph.getViewport().setMaxX(200);
                graph.getViewport().setMaxY(100);
                graph.getViewport().setScalable(true);
                graph.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                graph.setTitle("CDF");
                graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(){
                    @Override
                    public String formatLabel(double value, boolean isValueX){
                        if(isValueX){
                            return super.formatLabel(value, isValueX) + " m";
                        }else  {
                            return super.formatLabel(value, isValueX) + " %";
                        }
                    }
                });

                PointsGraphSeries<DataPoint> xywerte;
                xywerte = new PointsGraphSeries<>();
                for(DataPoint dp : dataPointArrayList){
                    xywerte.appendData(dp,false,1000);
                }
                //Radius für die Kreise des Graphens
                xywerte.setSize(5f);
                graph.addSeries(xywerte);
            }
        };

        activity.runOnUiThread(runnable);
    }


    public CharSequence createTimestamp(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        Date d = c.getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS");
        return sdf.format(d);
    }
    public CharSequence createDate(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        Date d = c.getTime();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy");
        return sdf.format(d);
    }

    /**
     * Listener für Start der MapActivity
     *
     * @param button ShowMapButton
     */
    void addShowMapButtonListener(Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(location != null) {
                    startMapActivity(interpolatedCoordinates, gpsCoordinates);
                }
            }
        });
    }

    /**
     * Listener für Routenstart
     *
     * @param button StartRouteButton
     */
    void addStartRouteButtonListener(final Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            int milestoneCounter = 0;
            @Override
            public void onClick(View view) {
                if(guiGPSComponents.getSeekBarValue() > 0) {
                    if (interpolatedCoordinates.isEmpty() && location != null && !started) {
                        gpsTimerDelayMillis = guiGPSComponents.getSeekBarValue() * 1000;
                        started = true;
                        timeStart = System.currentTimeMillis();
                        gpsTimerHandler.postDelayed(gpsTimerRunnable, 0);
                        button.setText("milestone" + (milestoneCounter + 1));
                        Toast toast = Toast.makeText(DistanceBasedActivity.this, "Route gestartet!", Toast.LENGTH_SHORT);
                        toast.show();
                    } else if (started) {
                        timeEnd = System.currentTimeMillis();
                      //  milestoneBuffer.setLongitude(mapCoordinates.get(milestoneCounter));
                        milestoneBufferA.setLatitude(mapCoordinates.get(milestoneCounter).latitude);
                        milestoneBufferA.setLongitude(mapCoordinates.get(milestoneCounter).longitude);
                        milestoneBufferB.setLatitude(mapCoordinates.get(milestoneCounter+1).latitude);
                        milestoneBufferB.setLongitude(mapCoordinates.get(milestoneCounter+1).longitude);

                        errorCalc.linearCoordinatesInterpolation(mapCoordinates.get(milestoneCounter), mapCoordinates.get(++milestoneCounter), 0, (long)((milestoneBufferA.distanceTo(milestoneBufferB)/guiGPSComponents.getSeekBarValue())*1000), 1000, interpolatedCoordinates);
                        //Toast toast = Toast.makeText(DistanceBasedActivity.this, "Ergebnis: " +(long)(milestoneBufferA.distanceTo(milestoneBufferB)/guiGPSComponents.getSeekBarValue())*1000, Toast.LENGTH_SHORT);
                        //toast.show();
                        button.setText("milestone" + (milestoneCounter + 1));
                        okToAdd = true;
                        timeStart = System.currentTimeMillis();
                    }
                }
                else{
                    Toast toast = Toast.makeText(DistanceBasedActivity.this, "Zeitintervall nicht gesetzt!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }

    /**
     * Listener für Routenstopp
     *
     * @param button StopRouteButton
     */
    void addStopRouteButtonListener(Button button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(location != null) {
                    gpsTimerHandler.removeCallbacks(gpsTimerRunnable);
                    Toast toast = Toast.makeText(DistanceBasedActivity.this, "Route gestoppt!\nCDF wird aktualisiert!", Toast.LENGTH_SHORT);
                    toast.show();
                    //computeError(interpolatedCoordinates, gpsCoordinates);
                    errorCalc.computeError(interpolatedCoordinates, gpsCoordinates);
                    ArrayList<DataPoint> dataPointArrayList;
                    dataPointArrayList = errorCalc.getDataPointArrayList();
                    createCDFGraph(dataPointArrayList);
                }

            }
        });
    }

    /**
     * converts an ArrayList of type LatLng in double[] for the transfer in an intent
     * @param intent intent in which to put
     * @param arrayListName name of the ArrayList - for the key of the value
     * @param valueType value name for the key
     * @param arrayList the ArrayList of type LatLng to convert
     */
    void convertArrayListForIntent(Intent intent, String arrayListName, String valueType, ArrayList<LatLng> arrayList){
        intent.putExtra(arrayListName + "Size", arrayList.size());
        for(int i = 0; i < arrayList.size(); i++){

            intent.putExtra(valueType + i, new double[]{arrayList.get(i).latitude, arrayList.get(i).longitude});
        }
    }

    public void radioOnClick(View v){
        int radioButtonID = guiRadioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioButtonID);

        switch (v.getId()){
            case R.id.radioLoc:
                gpsMode = 0;
                break;
            case R.id.radioBallanced:
                gpsMode = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
                break;
            case R.id.radioHigh:
                gpsMode = LocationRequest.PRIORITY_HIGH_ACCURACY;
                break;
            case R.id.radioLowP:
                gpsMode = LocationRequest.PRIORITY_LOW_POWER;
                break;
            case R.id.radioNoP:
                gpsMode = LocationRequest.PRIORITY_NO_POWER;
                break;
        }
        if(gpsMode!=0) {
            locationRequest.setPriority(gpsMode);
        }
        Toast.makeText(getBaseContext(),radioButton.getText()+ " mode:"+gpsMode ,Toast.LENGTH_SHORT).show();
    }
}
