package com.example.giuseppe.praktikumlm;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

public class MySensorEventListener implements SensorEventListener {

    private long interval = 1000000000;
    private final String sensorName;
    private final String[] bez;
    private LinkedList<EventData> events = new LinkedList<>();
    private GUISensorComponents components;

    public MySensorEventListener(String sensorName, String[] bez, GUISensorComponents components) {
        this.sensorName = sensorName;
        this.bez = bez;
        this.components = components;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(events.isEmpty())
            events.addLast(new EventData(sensorEvent.timestamp, sensorEvent.values));
        else if(sensorEvent.timestamp - events.getLast().timestamp >= interval)
            events.addLast(new EventData(sensorEvent.timestamp, sensorEvent.values));
        components.setResultTV(createResultString(sensorEvent));
    }

    private String createResultString(SensorEvent sensorEvent) {
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < bez.length; i++) {
            result.append(bez[i]).append(": ").append(sensorEvent.values[i]).append("\n");
        }
        return new String(result);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public JSONArray toJSONArray() {
        JSONArray result = new JSONArray();
        for(EventData event : events) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("timestamp", event.timestamp);
                for (int i = 0; i < bez.length; i++) {
                    obj.put(bez[i], event.values[i]);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("Error", "JSONException");
                //TODO: Handle Exception
            }
            result.put(obj);
        }
        return result;
    }

    public void saveToFile() {
        String dirname = "Sensor_saves/" + sensorName + "_saves";
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), dirname);

        if(!dir.exists()) {
           if(!dir.mkdirs()) {
               Log.e("Error", "Can't create Directory");
           }
        }

        int num = 1;
        String filename = sensorName + "_save" + num + ".json";
        File file = new File(dir, filename);

        while (file.exists()) {
            num++;
            filename = sensorName + "_save" + num + ".json";
            file = new File(dir, filename);
        }

        try {
            FileOutputStream fos = new FileOutputStream(file, false);
            fos.write(toJSONArray().toString().getBytes());
            fos.close();
            events = new LinkedList<>();
        }
        catch (FileNotFoundException e1) {
            e1.printStackTrace();
            Log.e("Error", "FileNotFoundException");
            //TODO: Handle Exception
        }
        catch (IOException e2) {
            e2.printStackTrace();
            Log.e("Error", "IOException");
            //TODO: Handle Exception
        }
    }

    public float[] getSensorValues() {
        return events.getLast().values;
    }
}
